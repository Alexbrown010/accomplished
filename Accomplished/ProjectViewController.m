//
//  ProjectViewController.m
//  Accomplished
//
//  Created by Alex Brown on 7/21/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ProjectViewController.h"

@interface ProjectViewController ()

@end

@implementation ProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //Display navigation bar
    self.navigationController.navigationBarHidden = YES;
    
    //Colors
    UIColor* color = [UIColor whiteColor];
    
    //Background view
    UIView* backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    backgroundView.backgroundColor = [UIColor colorWithRed:120/255.0f green:173/255.0f blue:0/255.0f alpha:5.0f];
    [self.view addSubview:backgroundView];
    
     UIView* backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    backgroundView1.backgroundColor = [UIColor whiteColor];
    [backgroundView addSubview:backgroundView1];
    
    //Black bar
    UIView* blackBar = [[UIView alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width, 2)];
    blackBar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:blackBar];
    
    //Back Button
    UIButton* backBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backBtn addTarget:self action:@selector(back:)
      forControlEvents:UIControlEventTouchUpInside];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [[backBtn layer] setBorderWidth:2.0f];
//    [[backBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    backBtn.frame = CGRectMake(10, 20, 50, 40);
    backBtn.layer.cornerRadius = 10;
    backBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backBtn];
    
    //Project label
    titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 85, self.view.frame.size.width, 40)];
    titleLbl.text = [self.infoDictionary objectForKey:@"ProjectTitle"];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.font = [titleLbl.font fontWithSize:30];
    [backgroundView addSubview:titleLbl];
    
    //Scroll view
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, 650, 180)];
    [self.view addSubview:scrollView];
    CGSize scrollViewContentSize = CGSizeMake(1000, 180);
    [scrollView setContentSize:scrollViewContentSize];
    [self.view addSubview:scrollView];
    
    // Project images
    NSString* user = [[PFUser currentUser] objectForKey:@"username"];

    UIImageView* firstImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 200, 180)];
    firstImage.contentMode = UIViewContentModeScaleAspectFit;
    firstImage.clipsToBounds = YES;
    //    [userImage.layer setBorderColor: [[UIColor blackColor] CGColor]];
    //    [userImage.layer setBorderWidth: 2.0];
    
    if ([user isEqual:@"AlexBrown010"]) {
        [firstImage setImage: [UIImage imageNamed:@"Frustration1"]];
    } else {
        //nothing
    }
    
    firstImage.alpha = 1.5;
    [scrollView addSubview:firstImage];
    
    UIImageView* secondImage = [[UIImageView alloc]initWithFrame:CGRectMake(220, 0, 200, 180)];
    secondImage.contentMode = UIViewContentModeScaleAspectFit;
    secondImage.clipsToBounds = YES;
    //    [userImage.layer setBorderColor: [[UIColor blackColor] CGColor]];
    //    [userImage.layer setBorderWidth: 2.0];
    
    if ([user isEqual:@"AlexBrown010"]) {
        [secondImage setImage: [UIImage imageNamed:@"frustration2"]];
    } else {
        //nothing
    }
    
    secondImage.alpha = 1.5;
    [scrollView addSubview:secondImage];
    
    UIImageView* thirdImage = [[UIImageView alloc]initWithFrame:CGRectMake(430, 0, 200, 180)];
    thirdImage.contentMode = UIViewContentModeScaleAspectFit;
    thirdImage.clipsToBounds = YES;
    //    [userImage.layer setBorderColor: [[UIColor blackColor] CGColor]];
    //    [userImage.layer setBorderWidth: 2.0];
    
    if ([user isEqual:@"AlexBrown010"]) {
        [thirdImage setImage: [UIImage imageNamed:@"Frustration3"]];
    } else {
        //nothing
    }
    
    thirdImage.alpha = 1.5;
    [scrollView addSubview:thirdImage];
    
//    //Project image
//    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 105, self.view.frame.size.width, 200)];
//    iv.contentMode = UIViewContentModeScaleAspectFit;
//    [iv setImage: [UIImage imageNamed: [self.infoDictionary objectForKey:@"Image"]]];
//    [scrollView addSubview:iv];
    
    //Project description lables
    UILabel* descriptionsLbl = [[UILabel alloc]initWithFrame:CGRectMake(40, 250, self.view.frame.size.width - 80, 40)];
    descriptionsLbl.text = @"Project Description";
    descriptionsLbl.textAlignment = NSTextAlignmentCenter;
    descriptionsLbl.lineBreakMode = NSLineBreakByWordWrapping;
    descriptionsLbl.numberOfLines = 0;
    //    [[directionsTxt layer] setBorderWidth:2.0f];
    //    [[directionsTxt layer] setBorderColor:[UIColor grayColor].CGColor];
    [backgroundView addSubview:descriptionsLbl];
    
        //Black bar
    UIView* blackBar2 = [[UIView alloc] initWithFrame:CGRectMake(0, 280, self.view.frame.size.width, 2)];
    blackBar2.backgroundColor = [UIColor blackColor];
    [backgroundView addSubview:blackBar2];
    
    UILabel* directionsTxt = [[UILabel alloc]initWithFrame:CGRectMake(10, 230, self.view.frame.size.width - 20,200)];
    directionsTxt.text = [self.infoDictionary objectForKey:@"ProjectDescription"];
    directionsTxt.textAlignment = NSTextAlignmentLeft;
    directionsTxt.font = [directionsTxt.font fontWithSize:12];
    directionsTxt.lineBreakMode = NSLineBreakByWordWrapping;
    directionsTxt.numberOfLines = 0;
//    [[directionsTxt layer] setBorderWidth:2.0f];
//    [[directionsTxt layer] setBorderColor:[UIColor grayColor].CGColor];
    [backgroundView addSubview:directionsTxt];
    
    //Indicator tableview
    indicatorTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 380, self.view.frame.size.width - 20, 200)];
    indicatorTableView.delegate = self;
    indicatorTableView.dataSource = self;
    indicatorTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:indicatorTableView];
    
    //Psuedo tabbar button
    UIButton* tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [tabBtn addTarget:self action:@selector(tab:)
     forControlEvents:UIControlEventTouchUpInside];
    [tabBtn setImage:[UIImage imageNamed:@"treeIcon"] forState: UIControlStateNormal];
    tabBtn.frame = CGRectMake(150, 610, self.view.frame.size.width - 300, 50);
    tabBtn.backgroundColor = [UIColor clearColor];
    [[tabBtn layer] setBorderWidth:1.0f];
    [[tabBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    tabBtn.layer.cornerRadius = 10;
    tabBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tabBtn];
    
    //Query for indicators
    PFQuery *indicatorQuery = [PFQuery queryWithClassName:@"Indicators"];
    [indicatorQuery whereKey:@"ProjectName" equalTo:titleLbl.text];
    [indicatorQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //NSLog(@"%@", objects);
            indicatorArray2 = [[NSArray alloc] initWithArray:objects];
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        
        [indicatorTableView reloadData];
        
    }];
    
    //Slide in new project creator
    addIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(400, 0, 400, 700)];
    addIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.8];
    [self.view addSubview:addIndicatorView];
    
    //Back Button
    UIButton* backBtn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backBtn2 addTarget:self action:@selector(back:)
      forControlEvents:UIControlEventTouchUpInside];
    [backBtn2 setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[backBtn2 layer] setBorderWidth:2.0f];
    [[backBtn2 layer] setBorderColor:[UIColor grayColor].CGColor];
    backBtn2.frame = CGRectMake(10, 20, 50, 40);
    backBtn2.layer.cornerRadius = 10;
    backBtn2.backgroundColor = [UIColor clearColor];
    [addIndicatorView addSubview:backBtn2];
    
    //New indicator label
    UILabel* newIndicatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 60, self.view.frame.size.width - 100, 80)];
    newIndicatorLbl.textAlignment = NSTextAlignmentCenter;
    newIndicatorLbl.textColor = [UIColor greenColor];
    newIndicatorLbl.backgroundColor = [UIColor clearColor];
    newIndicatorLbl.text = @"New Indicator";
    newIndicatorLbl.alpha = 3;
    newIndicatorLbl.font = [newIndicatorLbl.font fontWithSize:30];
    [addIndicatorView addSubview:newIndicatorLbl];
    
    //Add Indicator section
    UILabel* indicatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 195, self.view.frame.size.width - 120, 40)];
    indicatorLbl.textColor = [UIColor whiteColor];
    indicatorLbl.text = @"Project Indicator(s)";
    [addIndicatorView addSubview:indicatorLbl];
    
    //Open textview button
    UIButton* addIndicatorBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [addIndicatorBtn2 addTarget:self action:@selector(openIndicator:)
              forControlEvents:UIControlEventTouchUpInside];
    //    [addIndicatorBtn setImage:[UIImage imageNamed:@"smallPlus"] forState: UIControlStateNormal];
    [addIndicatorBtn2 setTitle:@"+" forState:UIControlStateNormal];
    [addIndicatorBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addIndicatorBtn2.titleLabel.font = [UIFont systemFontOfSize:40];
    //    addIndicatorBtn.layer.cornerRadius = 10;
    //    [[addIndicatorBtn layer] setBorderWidth:2.0f];
    //    [[addIndicatorBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    addIndicatorBtn2.frame = CGRectMake(285, 195, 40, 40);
    addIndicatorBtn2.backgroundColor = [UIColor clearColor];
    [addIndicatorView addSubview:addIndicatorBtn2];
    
    //Retrieve from parse
    UIButton* getIndicator = [UIButton buttonWithType:UIButtonTypeCustom];
    [getIndicator addTarget:self action:@selector(getIndicators:)
           forControlEvents:UIControlEventTouchUpInside];
    [getIndicator setTitle:@"-" forState:UIControlStateNormal];
    [getIndicator setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getIndicator.titleLabel.font = [UIFont systemFontOfSize:40];
    getIndicator.frame = CGRectMake(320, 195, 40, 40);
    //    getIndicator.backgroundColor = [UIColor clearColor];
    //    [[getIndicator layer] setBorderWidth:1.0f];
    //    [[getIndicator layer] setBorderColor:[UIColor whiteColor].CGColor];
    //    getIndicator.layer.cornerRadius = 10;
    getIndicator.backgroundColor = [UIColor clearColor];
    [addIndicatorView addSubview:getIndicator];
    
    //Open textview button
    greenLine3 = [[UIView alloc] initWithFrame:CGRectMake(10, 225, self.view.frame.size.width, 1)];
    greenLine3.backgroundColor = [UIColor greenColor];
    greenLine3.alpha = 3;
    [addIndicatorView addSubview:greenLine3];
    
    //Indicator Textview
    indicatorTxt = [[UITextView alloc] initWithFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 0)];
    indicatorTxt.textAlignment = NSTextAlignmentLeft;
    indicatorTxt.backgroundColor = color;
    indicatorTxt.textColor = [UIColor blackColor];
    [addIndicatorView addSubview:indicatorTxt];
    
    //Submit project Button
    UIButton* submitProjectBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [submitProjectBtn addTarget:self action:@selector(submit:)
               forControlEvents:UIControlEventTouchUpInside];
    [submitProjectBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitProjectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[submitProjectBtn layer] setBorderWidth:2.0f];
    [[submitProjectBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    submitProjectBtn.frame = CGRectMake(150, 525, self.view.frame.size.width - 300, 40);
    submitProjectBtn.layer.cornerRadius = 10;
    submitProjectBtn.backgroundColor = [UIColor clearColor];
    [addIndicatorView addSubview:submitProjectBtn];
    
}

//Get indicators
-(void)getIndicators:(id) sender
{
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [indicatorTxt setFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 0)]; } completion:nil];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine3 setFrame:CGRectMake(10, 295, self.view.frame.size.width, 1)]; } completion:nil];
    
    saveIndicatorBtn.hidden = YES;
    highPriorityBtn.hidden = YES;
    mediumPriorityBtn.hidden = YES;
    lowPriorityBtn.hidden = YES;
    
}

//Back button action
-(void)back:(id) sender
{
    ProfileViewController* pvc = [ProfileViewController new];
    [self.navigationController pushViewController:pvc animated:YES];
    
}

-(void)tab:(id) sender
{
    
    static int count = 2;
    count ++;
    if (count % 2 !=0) {
        
        //Add Indicator button
        
        addIndicatorBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [addIndicatorBtn addTarget:self action:@selector(addIndicator:)
          forControlEvents:UIControlEventTouchUpInside];
        [addIndicatorBtn setTitle:@"Add" forState:UIControlStateNormal];
        [addIndicatorBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [[addIndicatorBtn layer] setBorderWidth:1.0f];
        [[addIndicatorBtn layer] setBorderColor:[UIColor blackColor].CGColor];
        addIndicatorBtn.frame = CGRectMake(250, 610, 110, 50);
        addIndicatorBtn.layer.cornerRadius = 10;
        addIndicatorBtn.backgroundColor = [UIColor clearColor];
        [self.view addSubview:addIndicatorBtn];

        
        //Complete project button
        completeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [completeBtn addTarget:self action:@selector(complete:)
              forControlEvents:UIControlEventTouchUpInside];
        [completeBtn setTitle:@"Complete" forState:UIControlStateNormal];
        [completeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [[completeBtn layer] setBorderWidth:1.0f];
        [[completeBtn layer] setBorderColor:[UIColor blackColor].CGColor];
        completeBtn.frame = CGRectMake(10, 610, 110, 50);
        completeBtn.layer.cornerRadius = 10;
        completeBtn.backgroundColor = [UIColor clearColor];
        [self.view addSubview:completeBtn];
        
    }
    
    else {
        
        completeBtn.frame = CGRectMake(0, 0, 0, 0);
        addIndicatorBtn.frame = CGRectMake(0, 0, 0, 0);
        
    }
    
}

//Open indicator textview
-(void)openIndicator:(id) sender
{
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [indicatorTxt setFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 100)]; } completion:nil];
    
    highPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [highPriorityBtn addTarget:self action:@selector(highPriority:)
              forControlEvents:UIControlEventTouchUpInside];
    [highPriorityBtn setTitle:@"High" forState:UIControlStateNormal];
    [highPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[highPriorityBtn layer] setBorderWidth:2.0f];
    [[highPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    highPriorityBtn.frame = CGRectMake(20, 430, 80, 60);
    highPriorityBtn.layer.cornerRadius = 10;
    highPriorityBtn.backgroundColor = [UIColor redColor];
    [addIndicatorView addSubview:highPriorityBtn];
    
    mediumPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mediumPriorityBtn addTarget:self action:@selector(mediumPriority:)
                forControlEvents:UIControlEventTouchUpInside];
    [mediumPriorityBtn setTitle:@"Medium" forState:UIControlStateNormal];
    [mediumPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[mediumPriorityBtn layer] setBorderWidth:2.0f];
    [[mediumPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    mediumPriorityBtn.frame = CGRectMake(140, 430, 80, 60);
    mediumPriorityBtn.layer.cornerRadius = 10;
    mediumPriorityBtn.backgroundColor = [UIColor yellowColor];
    [addIndicatorView addSubview:mediumPriorityBtn];
    
    lowPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [lowPriorityBtn addTarget:self action:@selector(lowPriority:)
             forControlEvents:UIControlEventTouchUpInside];
    [lowPriorityBtn setTitle:@"Low" forState:UIControlStateNormal];
    [lowPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[lowPriorityBtn layer] setBorderWidth:2.0f];
    [[lowPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    lowPriorityBtn.frame = CGRectMake(260, 430, 80, 60);
    lowPriorityBtn.layer.cornerRadius = 10;
    lowPriorityBtn.backgroundColor = [UIColor greenColor];
    [addIndicatorView addSubview:lowPriorityBtn];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine3 setFrame:CGRectMake(10, 515, self.view.frame.size.width, 1)]; } completion:nil];
    
}

//Submit project
-(void)submit:(id) sender
{
    
    PFObject* newIndicator = [PFObject objectWithClassName:@"Indicators"];
    newIndicator[@"IndicatorDescription"] = indicatorTxt.text;
    newIndicator[@"ProjectName"] = [self.infoDictionary objectForKey:@"ProjectTitle"];
    newIndicator[@"Priority"] = priority2;
    
    [newIndicator saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            UIAlertView* succesAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your project indicator has been saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [succesAlert show];
        }
        
        else {
            
            UIAlertView* failureAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your indicator was not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [failureAlert show];
        }
        
    }];
    
    indicatorTxt.text = @" ";
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [addIndicatorView setFrame:CGRectMake(400, 0, 400, 700)]; } completion:nil];
    
    [indicatorTableView reloadData];
    
}

-(void)highPriority:(id) sender
{
    priority2 = @"High";
}

-(void)mediumPriority:(id) sender
{
    priority2 = @"Medium";
}

-(void)lowPriority:(id) sender
{
    priority2 = @"Low";
}

//Complete button action
-(void)complete:(id) sender
{
        
        UIAlertView* congratsAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"Well done you completed a project!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [congratsAlert show];
        
        PFObject* completedProject = [PFObject objectWithClassName:@"CompletedProjects"];
        completedProject[@"ProjectTitle"] = [self.infoDictionary objectForKey:@"ProjectTitle"];
        completedProject[@"UserID"] = [self.infoDictionary objectForKey:@"UserID"];
    
        [completedProject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (!error) {
                
            }
            
            else {
                
                UIAlertView* failureAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your project was not completed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [failureAlert show];
            }
        }];
    
    NSString* objectId = [self.infoDictionary objectForKey:@"objectId"];
    
        PFObject *object = [PFObject objectWithoutDataWithClassName:@"Projects"
                                                           objectId:objectId];
        [object deleteEventually];
        
        ProfileViewController* pvc = [ProfileViewController new];
        [self.navigationController pushViewController:pvc animated:YES];
    
}

//Add indicator button
-(void)addIndicator:(id) sender
{
    
    [self.view addSubview:addIndicatorView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [addIndicatorView setFrame:CGRectMake(0, 0, 400, 700)]; } completion:nil];
    
}

//UITablview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return indicatorArray2.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    PFObject* tempObject = [indicatorArray2 objectAtIndex:indexPath.row];
    
    UILabel *textLbl = [[UILabel alloc]initWithFrame:CGRectMake(35, 10, self.view.frame.size.width - 30, 30)];
    textLbl.text = [tempObject objectForKey:@"IndicatorDescription"];
    textLbl.font = [textLbl.font fontWithSize:15];
    textLbl.backgroundColor = [UIColor clearColor];
    textLbl.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:textLbl];
    
//    cell.textLabel.text = [tempObject objectForKey:@"IndicatorDescription"];
//    cell.textLabel.font = [UIFont systemFontOfSize:15.0];
//    cell.textLabel.textColor = [UIColor whiteColor];
    //cell.indentationLevel = cell.indentationLevel + 2;
    
    NSString* testPriority = [tempObject objectForKey:@"Priority"];
    if ([testPriority isEqual:@"High"]) {
        
        UIImageView* red = [[UIImageView alloc] initWithFrame:CGRectMake(8, 9, 30, 30)];
        red.backgroundColor=[UIColor clearColor];
        [red.layer setCornerRadius:8.0f];
        [red.layer setMasksToBounds:YES];
        [red setImage:[UIImage imageNamed:@"red"]];
        [cell.contentView addSubview:red];
        
        //NSLog(@"High");
        
    } else if ([testPriority isEqual:@"Medium"]) {
        
        UIImageView* yellow = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
        yellow.backgroundColor=[UIColor clearColor];
        [yellow.layer setCornerRadius:8.0f];
        [yellow.layer setMasksToBounds:YES];
        [yellow setImage:[UIImage imageNamed:@"yellow"]];
        [cell.contentView addSubview:yellow];
        
    } else {
        
        //NSLog(@"Low");
        
        UIImageView* green = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
        green.backgroundColor=[UIColor clearColor];
        [green.layer setCornerRadius:8.0f];
        [green.layer setMasksToBounds:YES];
        [green setImage:[UIImage imageNamed:@"green"]];
        [cell.contentView addSubview:green];
        
    }
    
//    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"blackbox"]];
//    [cell.accessoryView setFrame:CGRectMake(0, 0, 30, 30)];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
