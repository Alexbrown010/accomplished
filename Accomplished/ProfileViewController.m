//
//  ProfileViewController.m
//  Accomplished
//
//  Created by Alex Brown on 7/17/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Display navigation bar
    self.navigationController.navigationBarHidden = YES;
    
    //Property List
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DemoProjects" ofType:@"plist"];
    arrayOfProjects = [[NSArray alloc]initWithContentsOfFile:filePath];
    
    //Colors
    UIColor* color = [UIColor whiteColor];
    
    //Background views
    UIView* backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    backgroundView1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backgroundView1];
    
    UIView* backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 72, self.view.frame.size.width, self.view.frame.size.height)];
    backgroundView2.backgroundColor = [UIColor colorWithRed:120/255.0f green:173/255.0f blue:0/255.0f alpha:5.0f];
    [self.view addSubview:backgroundView2];
    
    //Log user out
    UIButton* logOutBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [logOutBtn addTarget:self action:@selector(logUserOut:)
            forControlEvents:UIControlEventTouchUpInside];
    [logOutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logOutBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [[logOutBtn layer] setBorderWidth:1.0f];
//    [[logOutBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    logOutBtn.backgroundColor = [UIColor clearColor];
    logOutBtn.frame = CGRectMake(10, 25, 50, 40);
    logOutBtn.layer.cornerRadius = 10;
    [self.view addSubview:logOutBtn];
    
     //Add project button
    UIButton* addProjectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addProjectBtn addTarget:self action:@selector(add:)
                   forControlEvents:UIControlEventTouchUpInside];
    [addProjectBtn setImage:[UIImage imageNamed:@"plus"] forState: UIControlStateNormal];
//    [[addProjectBtn layer] setBorderWidth:1.0f];
//    [[addProjectBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
//    addProjectBtn.layer.cornerRadius = 10;
    addProjectBtn.frame = CGRectMake(320, 25, 40, 40);
    addProjectBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:addProjectBtn];
    
    //Black Bar
    UIView* blackBar = [[UIView alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width, 2)];
    blackBar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:blackBar];
    
        //Slide in new project creator
    newProjectView = [[UIView alloc] initWithFrame:CGRectMake(400, 0, 400, 700)];
    newProjectView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.8];
    [backgroundImage addSubview:newProjectView];
    
            //Back Button
    UIButton* backBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backBtn addTarget:self action:@selector(back:)
      forControlEvents:UIControlEventTouchUpInside];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[backBtn layer] setBorderWidth:2.0f];
    [[backBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    backBtn.frame = CGRectMake(10, 20, 50, 40);
    backBtn.layer.cornerRadius = 10;
    backBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:backBtn];
    
        //New project label
    UILabel* newProjectLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 60, self.view.frame.size.width - 100, 80)];
    newProjectLbl.textAlignment = NSTextAlignmentCenter;
    newProjectLbl.textColor = [UIColor greenColor];
    newProjectLbl.backgroundColor = [UIColor clearColor];
    newProjectLbl.text = @"New Project";
    newProjectLbl.alpha = 3;
    newProjectLbl.font = [newProjectLbl.font fontWithSize:30];
    [newProjectView addSubview:newProjectLbl];
    
            //Project title textfield
    projectTitleTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 150, self.view.frame.size.width, 40)];
    projectTitleTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Project Title" attributes:@{NSForegroundColorAttributeName: color}];
    projectTitleTxt.textAlignment = NSTextAlignmentLeft;
    projectTitleTxt.textColor = [UIColor whiteColor];
    projectTitleTxt.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:projectTitleTxt];
                //Green border
    greenLine = [[UIView alloc] initWithFrame:CGRectMake(10, 195, self.view.frame.size.width, 1)];
    greenLine.backgroundColor = [UIColor greenColor];
    greenLine.alpha = 3;
    [newProjectView addSubview:greenLine];
    
            //Description Label and textview with animations
    UILabel* descriptionLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 200, self.view.frame.size.width - 120, 40)];
    descriptionLbl.textColor = [UIColor whiteColor];
    descriptionLbl.text = @"Project Description";
    [newProjectView addSubview:descriptionLbl];
                //Open textview button
    UIButton* openTVBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [openTVBtn addTarget:self action:@selector(open:)
      forControlEvents:UIControlEventTouchUpInside];
    //[openTVBtn setImage:[UIImage imageNamed:@"smallPlus"] forState: UIControlStateNormal];
    [openTVBtn setTitle:@"+" forState:UIControlStateNormal];
    [openTVBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    openTVBtn.titleLabel.font = [UIFont systemFontOfSize:40];
//    [[openTVBtn layer] setBorderWidth:2.0f];
//    [[openTVBtn layer] setBorderColor:[UIColor grayColor].CGColor];
//    openTVBtn.layer.cornerRadius = 10;
    openTVBtn.frame = CGRectMake(285, 200, 40, 40);
    openTVBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:openTVBtn];
                //Open textview button
    UIButton* closeTVBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeTVBtn addTarget:self action:@selector(close:)
        forControlEvents:UIControlEventTouchUpInside];
    //[closeTVBtn setImage:[UIImage imageNamed:@"minus1"] forState: UIControlStateNormal];
    [closeTVBtn setTitle:@"-" forState:UIControlStateNormal];
    [closeTVBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    closeTVBtn.titleLabel.font = [UIFont systemFontOfSize:40];
//    [[closeTVBtn layer] setBorderWidth:1.0f];
//    [[closeTVBtn layer] setBorderColor:[UIColor grayColor].CGColor];
//    closeTVBtn.layer.cornerRadius = 10;
    closeTVBtn.frame = CGRectMake(320, 200, 40, 40);
    closeTVBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:closeTVBtn];
                //Green border
    greenLine2 = [[UIView alloc] initWithFrame:CGRectMake(10, 245, self.view.frame.size.width, 1)];
    greenLine2.backgroundColor = [UIColor greenColor];
    greenLine2.alpha = 3;
    [newProjectView addSubview:greenLine2];
    
                //Description textview
    descriptionTxt = [[UITextView alloc] initWithFrame:CGRectMake(10, 255, self.view.frame.size.width - 20, 0)];
    //descriptionTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Project Description" attributes:@{NSForegroundColorAttributeName: color}];
    descriptionTxt.textAlignment = NSTextAlignmentLeft;
    descriptionTxt.backgroundColor = color;
    descriptionTxt.textColor = [UIColor blackColor];
    [newProjectView addSubview:descriptionTxt];
    
        //Add Indicator section
    UILabel* indicatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 250, self.view.frame.size.width - 120, 40)];
    indicatorLbl.textColor = [UIColor whiteColor];
    indicatorLbl.text = @"Project Indicator(s)";
    [newProjectView addSubview:indicatorLbl];
    
            //Open textview button
    UIButton* addIndicatorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addIndicatorBtn addTarget:self action:@selector(openIndicator:)
        forControlEvents:UIControlEventTouchUpInside];
//    [addIndicatorBtn setImage:[UIImage imageNamed:@"smallPlus"] forState: UIControlStateNormal];
    [addIndicatorBtn setTitle:@"+" forState:UIControlStateNormal];
    [addIndicatorBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addIndicatorBtn.titleLabel.font = [UIFont systemFontOfSize:40];
//    addIndicatorBtn.layer.cornerRadius = 10;
//    [[addIndicatorBtn layer] setBorderWidth:2.0f];
//    [[addIndicatorBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    addIndicatorBtn.frame = CGRectMake(285, 250, 40, 40);
    addIndicatorBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:addIndicatorBtn];
    
    //Retrieve from parse
    UIButton* getIndicator = [UIButton buttonWithType:UIButtonTypeCustom];
    [getIndicator addTarget:self action:@selector(getIndicators:)
           forControlEvents:UIControlEventTouchUpInside];
    [getIndicator setTitle:@"-" forState:UIControlStateNormal];
    [getIndicator setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getIndicator.titleLabel.font = [UIFont systemFontOfSize:40];
    getIndicator.frame = CGRectMake(320, 250, 40, 40);
//    getIndicator.backgroundColor = [UIColor clearColor];
//    [[getIndicator layer] setBorderWidth:1.0f];
//    [[getIndicator layer] setBorderColor:[UIColor whiteColor].CGColor];
//    getIndicator.layer.cornerRadius = 10;
    getIndicator.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:getIndicator];
    
            //Open textview button
    greenLine3 = [[UIView alloc] initWithFrame:CGRectMake(10, 295, self.view.frame.size.width, 1)];
    greenLine3.backgroundColor = [UIColor greenColor];
    greenLine3.alpha = 3;
    [newProjectView addSubview:greenLine3];
    
            //Indicator Textview
    indicatorTxt = [[UITextView alloc] initWithFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 0)];
    indicatorTxt.textAlignment = NSTextAlignmentLeft;
    indicatorTxt.backgroundColor = color;
    indicatorTxt.textColor = [UIColor blackColor];
    [newProjectView addSubview:indicatorTxt];
    
            //Image Button
    UIButton* imageBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [imageBtn addTarget:self action:@selector(addImage:)
              forControlEvents:UIControlEventTouchUpInside];
    [imageBtn setTitle:@"Image" forState:UIControlStateNormal];
    [imageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[imageBtn layer] setBorderWidth:2.0f];
    [[imageBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    imageBtn.frame = CGRectMake(150, 570, self.view.frame.size.width - 300, 40);
    imageBtn.layer.cornerRadius = 10;
    imageBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:imageBtn];
    
            //Submit project Button
    UIButton* submitProjectBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [submitProjectBtn addTarget:self action:@selector(submit:)
       forControlEvents:UIControlEventTouchUpInside];
    [submitProjectBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitProjectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[submitProjectBtn layer] setBorderWidth:2.0f];
    [[submitProjectBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    submitProjectBtn.frame = CGRectMake(150, 620, self.view.frame.size.width - 300, 40);
    submitProjectBtn.layer.cornerRadius = 10;
    submitProjectBtn.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:submitProjectBtn];
    
            //Indicator tableview
    tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    tableview.delegate = self;
    tableview.dataSource = self;
    tableview.backgroundColor = [UIColor clearColor];
    [newProjectView addSubview:tableview];
    
    //User image and image picker
    NSString* user = [[PFUser currentUser] objectForKey:@"username"];
    
    userImage = [[UIImageView alloc]initWithFrame:CGRectMake(105, 85, self.view.frame.size.width - 210, 150)];
    userImage.contentMode = UIViewContentModeScaleAspectFit;
    userImage.clipsToBounds = YES;
//    [userImage.layer setBorderColor: [[UIColor blackColor] CGColor]];
//    [userImage.layer setBorderWidth: 2.0];
    
    if ([user isEqual:@"AlexBrown010"]) {
        [userImage setImage: [UIImage imageNamed:@"Goku2"]];
    } else {
        [userImage setImage: [UIImage imageNamed:@"portrait-placeholder-2"]];
    }
    
    userImage.alpha = 1.5;
    [self.view addSubview:userImage];
        //image picker
    UIButton* profileImageBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [profileImageBtn addTarget:self action:@selector(profileImage:) forControlEvents:UIControlEventTouchUpInside];
    profileImageBtn.frame = CGRectMake(90, 75, self.view.frame.size.width - 180, 160);
    [self.view addSubview:profileImageBtn];
    
    //PFQuery
    
    //title = [[PFUser currentUser] objectForKey:@"ProjectTitle"];
    
//    UILabel testLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
//    testLabel.text = title;
//    [self.view addSubview:testLabel];
    
    //Completed projects button
    UIButton* completedProjectsBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [completedProjectsBtn addTarget:self action:@selector(completed:)
     forControlEvents:UIControlEventTouchUpInside];
    [completedProjectsBtn setTitle:@"Completed Projects" forState:UIControlStateNormal];
    [completedProjectsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[completedProjectsBtn layer] setBorderWidth:0.5f];
    completedProjectsBtn.tintColor = [UIColor lightGrayColor];
    [[completedProjectsBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    completedProjectsBtn.frame = CGRectMake(20, 240, 150, 40);
    completedProjectsBtn.layer.cornerRadius = 10;
    completedProjectsBtn.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:completedProjectsBtn];
    
    //Active projects button
    UIButton* activeProjectsBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [activeProjectsBtn addTarget:self action:@selector(active:)
                   forControlEvents:UIControlEventTouchUpInside];
    [activeProjectsBtn setTitle:@"Active Projects" forState:UIControlStateNormal];
    [activeProjectsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[activeProjectsBtn layer] setBorderWidth:0.5f];
    [[activeProjectsBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    activeProjectsBtn.frame = CGRectMake(200, 240, self.view.frame.size.width - 220, 40);
    activeProjectsBtn.layer.cornerRadius = 10;
    activeProjectsBtn.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:activeProjectsBtn];
    
//    //Swipe View
//    swipeView = [[UIView alloc] initWithFrame:CGRectMake(0, 290, self.view.frame.size.width, 315)];
//    swipeView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:swipeView];
//    
//    //Swipe gesture recognizers
//    UISwipeGestureRecognizer* swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
//    [swipeRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    [swipeView addGestureRecognizer:swipeRight];
//    
//    UISwipeGestureRecognizer* swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
//    [swipeLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//    [swipeView addGestureRecognizer:swipeLeft];
    
    //Collection view
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    collectionView1 = [[UICollectionView alloc] initWithFrame:CGRectMake(30, 215, self.view.frame.size.width - 60, 315) collectionViewLayout:layout];
    [collectionView1 setDataSource:self];
    [collectionView1 setDelegate:self];
    [collectionView1 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView1 setBackgroundColor:[UIColor clearColor]];
    [backgroundView2 addSubview:collectionView1];
    
    //Psuedo tabbar button
    UIButton* tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [tabBtn addTarget:self action:@selector(tab:)
       forControlEvents:UIControlEventTouchUpInside];
    [tabBtn setImage:[UIImage imageNamed:@"treeIcon"] forState: UIControlStateNormal];
    tabBtn.frame = CGRectMake(150, 610, self.view.frame.size.width - 300, 50);
    tabBtn.backgroundColor = [UIColor clearColor];
    [[tabBtn layer] setBorderWidth:1.0f];
    [[tabBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    tabBtn.layer.cornerRadius = 10;
    tabBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tabBtn];
    
}

//Display completed projects
-(void)completed:(id) sender
{
    userId = [[PFUser currentUser] objectForKey:@"username"];
    
    PFQuery *projectQuery1 = [PFQuery queryWithClassName:@"CompletedProjects"];
    [projectQuery1 whereKey:@"UserID" equalTo:userId];
    [projectQuery1 findObjectsInBackgroundWithBlock:^(NSArray *project, NSError *error) {
        if (!error) {
            //NSLog(@"%@", project);
            projectsArray = [[NSArray alloc] initWithArray:project];
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        
        [collectionView1 reloadData];
    }];
}

//Display active projects
-(void)active:(id) sender
{
    
    userId = [[PFUser currentUser] objectForKey:@"username"];
    
    PFQuery *projectQuery1 = [PFQuery queryWithClassName:@"Projects"];
    [projectQuery1 whereKey:@"UserID" equalTo:userId];
    [projectQuery1 findObjectsInBackgroundWithBlock:^(NSArray *project, NSError *error) {
        if (!error) {
            //NSLog(@"%@", project);
            projectsArray = [[NSArray alloc] initWithArray:project];
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        
        [collectionView1 reloadData];
    }];
}

//Get indicators
-(void)getIndicators:(id) sender
{
        PFQuery *indicatorQuery = [PFQuery queryWithClassName:@"Indicators"];
        [indicatorQuery whereKey:@"ProjectName" equalTo:projectTitleTxt.text];
        [indicatorQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                //NSLog(@"%@", objects);
                indicatorArray = [[NSArray alloc] initWithArray:objects];
                
            } else {
                // Log details of the failure
                NSLog(@"Error: %@ %@", error, [error userInfo]);
            }
            
            [tableview reloadData];
            
        }];

    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [indicatorTxt setFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 0)]; } completion:nil];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine3 setFrame:CGRectMake(10, 295, self.view.frame.size.width, 1)]; } completion:nil];
    
    saveIndicatorBtn.hidden = YES;
    highPriorityBtn.hidden = YES;
    mediumPriorityBtn.hidden = YES;
    lowPriorityBtn.hidden = YES;
    priorityLbl.hidden = YES;
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [tableview setFrame:CGRectMake(0, 305, self.view.frame.size.width, 255)]; } completion:nil];
    
}

//Log user out
-(void)logUserOut:(id) sender
{
 
    [PFUser logOut];
    
    ViewController *vc = [[ViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"You have been logged out" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [logoutAlert show];

}

//Profile user image
-(void)profileImage:(id) sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}

//Image Picker controller
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
         userImage.image = (UIImage*) [info objectForKey:UIImagePickerControllerOriginalImage];

 }
 
//Slide in view
-(void)add:(id) sender
{
    [self.view addSubview:newProjectView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [newProjectView setFrame:CGRectMake(0, 0, 400, 700)]; } completion:nil];
}

//Back Button Action
-(void)back:(id) sender
{
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [newProjectView setFrame:CGRectMake(400, 0, 400, 700)]; } completion:nil];
    
    projectTitleTxt.text = @" ";
    descriptionTxt.text = @" ";
    tableview.hidden = YES;
}

//Open textview
-(void)open:(id) sender
{
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [descriptionTxt setFrame:CGRectMake(10, 255, self.view.frame.size.width - 20, 200)]; } completion:nil];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine2 setFrame:CGRectMake(10, 465, self.view.frame.size.width, 1)]; } completion:nil];
    
}

//Close textview
-(void)close:(id) sender
{
 
    [newProjectView addSubview:descriptionTxt];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{ [descriptionTxt setFrame:CGRectMake(10, 255, self.view.frame.size.width - 20, 0)]; } completion:nil];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine2 setFrame:CGRectMake(10, 245, self.view.frame.size.width, 1)]; } completion:nil];
}

//Open indicator textview
-(void)openIndicator:(id) sender
{
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [indicatorTxt setFrame:CGRectMake(10, 305, self.view.frame.size.width - 20, 100)]; } completion:nil];
    
    priorityLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 410, self.view.frame.size.width - 100, 30)];
    priorityLbl.text = @"Priority Level";
    priorityLbl.textColor = [UIColor greenColor];
    priorityLbl.textAlignment = NSTextAlignmentCenter;
    [newProjectView addSubview:priorityLbl];
    
    highPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [highPriorityBtn addTarget:self action:@selector(highPriority:)
               forControlEvents:UIControlEventTouchUpInside];
    [highPriorityBtn setTitle:@"High" forState:UIControlStateNormal];
    [highPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[highPriorityBtn layer] setBorderWidth:2.0f];
    [[highPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    highPriorityBtn.frame = CGRectMake(20, 440, 80, 60);
    highPriorityBtn.layer.cornerRadius = 10;
    highPriorityBtn.backgroundColor = [UIColor redColor];
    [newProjectView addSubview:highPriorityBtn];
    
    mediumPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mediumPriorityBtn addTarget:self action:@selector(mediumPriority:)
              forControlEvents:UIControlEventTouchUpInside];
    [mediumPriorityBtn setTitle:@"Medium" forState:UIControlStateNormal];
    [mediumPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[mediumPriorityBtn layer] setBorderWidth:2.0f];
    [[mediumPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    mediumPriorityBtn.frame = CGRectMake(140, 440, 80, 60);
    mediumPriorityBtn.layer.cornerRadius = 10;
    mediumPriorityBtn.backgroundColor = [UIColor yellowColor];
    [newProjectView addSubview:mediumPriorityBtn];
    
    lowPriorityBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [lowPriorityBtn addTarget:self action:@selector(lowPriority:)
              forControlEvents:UIControlEventTouchUpInside];
    [lowPriorityBtn setTitle:@"Low" forState:UIControlStateNormal];
    [lowPriorityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[lowPriorityBtn layer] setBorderWidth:2.0f];
    [[lowPriorityBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    lowPriorityBtn.frame = CGRectMake(260, 440, 80, 60);
    lowPriorityBtn.layer.cornerRadius = 10;
    lowPriorityBtn.backgroundColor = [UIColor greenColor];
    [newProjectView addSubview:lowPriorityBtn];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [greenLine3 setFrame:CGRectMake(10, 515, self.view.frame.size.width, 1)]; } completion:nil];
    
    saveIndicatorBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [saveIndicatorBtn addTarget:self action:@selector(saveIndicator:)
               forControlEvents:UIControlEventTouchUpInside];
    [saveIndicatorBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveIndicatorBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[saveIndicatorBtn layer] setBorderWidth:2.0f];
    [[saveIndicatorBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    saveIndicatorBtn.frame = CGRectMake(150, 525, self.view.frame.size.width - 300, 40);
    saveIndicatorBtn.layer.cornerRadius = 10;
    saveIndicatorBtn.backgroundColor = [UIColor redColor];
    [newProjectView addSubview:saveIndicatorBtn];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromTop animations:^{ [tableview setFrame:CGRectMake(0, 305, self.view.frame.size.width, 0)]; } completion:nil];
    
}

//Save indicator button
-(void)saveIndicator:(id) sender
{
    
    PFObject* newIndicator = [PFObject objectWithClassName:@"Indicators"];
    newIndicator[@"IndicatorDescription"] = indicatorTxt.text;
    newIndicator[@"ProjectName"] = projectTitleTxt.text;
    newIndicator[@"Priority"] = priority;
    
    [newIndicator saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (!error) {
            
            UIAlertView* succesAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your project indicator has been saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [succesAlert show];
        }
        
        else {
            
            UIAlertView* failureAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your indicator was not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [failureAlert show];
        }
        
    }];
    
    indicatorTxt.text = @" ";
    
}

//Add project image
-(void)addImage:(id) sender
{
    
}

//Submit project
-(void)submit:(id) sender
{
    [collectionView1 reloadData];
    
    userId = [[PFUser currentUser] objectForKey:@"username"];
    
    PFObject* newProject = [PFObject objectWithClassName:@"Projects"];
    newProject[@"ProjectTitle"] = projectTitleTxt.text;
    newProject[@"ProjectDescription"] = descriptionTxt.text;
    newProject[@"UserID"] = userId;
    
    [newProject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

    if (!error) {
         
         UIAlertView* succesAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your project has been saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
         
         [succesAlert show];
     }
     
     else {
     
         UIAlertView* failureAlert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Your project was not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
         
         [failureAlert show];
     }
        }];
    
    PFQuery *projectQuery = [PFQuery queryWithClassName:@"Projects"];
    [projectQuery whereKey:@"UserID" equalTo:userId];
    [projectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //NSLog(@"%@", objects);
            projectsArray = [[NSArray alloc] initWithArray:objects];
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    
        //[collectionView1 reloadData];
        
    }];
   
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [newProjectView setFrame:CGRectMake(400, 0, 400, 700)]; } completion:nil];
    
}

-(void)highPriority:(id) sender
{
    priority = @"High";
}

-(void)mediumPriority:(id) sender
{
    priority = @"Medium";
}

-(void)lowPriority:(id) sender
{
    priority = @"Low";
}


//-(void)swipeHandlerRight:(UISwipeGestureRecognizer *)sender
//{
//    //NSLog(@"swiped right");
//    //[self.view addSubview:swipeView];
//    
//    [UIView animateWithDuration:1.0 delay:0.0f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [swipeView setFrame:CGRectMake(423, 290, 300, 315)]; } completion:nil];
//}

//-(void)swipeHandlerLeft:(UISwipeGestureRecognizer *)sender
//{
//    //NSLog(@"swiped left");
//    //[self.view addSubview:swipeView];
//    
//    [UIView animateWithDuration:1.0 delay:0.0f options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{ [swipeView setFrame:CGRectMake(-423, 290, 300, 315)]; } completion:nil];
//}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return 10;
    return projectsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
//    NSInteger randomGen = arc4random_uniform(4);
//    
//    if (randomGen == 1) {
//    
//        cell.backgroundColor = [UIColor colorWithRed:249/255.0f green:249/255.0f blue:209/255.0f alpha:1.0f];
//    
//    } else if (randomGen == 2) {
//        
//        cell.backgroundColor = [UIColor colorWithRed:191/255.0f green:240/255.0f blue:248/255.0f alpha:1.0f];
//        
//    } else {
//        
//        cell.backgroundColor = [UIColor colorWithRed:239/255.0f green:198/255.0f blue:255/255.0f alpha:1.0f];
//        
//    }
    
    for (UIView *view2 in cell.contentView.subviews) {
        [view2 removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor colorWithRed:224/255.0f green:249/255.0f blue:209/255.0f alpha:1.0f];
    //[UIColor colorWithRed:120/255.0f green:173/255.0f blue:0/255.0f alpha:5.0f];
    
    PFObject* tempObject = [projectsArray objectAtIndex:indexPath.row];
    
    UILabel *titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, cell.bounds.size.width, 40)];
    titleLbl.tag = 200;
    titleLbl.font = [UIFont systemFontOfSize:20];
    titleLbl.text = [tempObject objectForKey:@"ProjectTitle"];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.textColor = [UIColor blackColor];
    [cell.contentView addSubview:titleLbl];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(150, 150);
}

//CollectionView select item at index path
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = projectsArray[indexPath.row];
    
    ProjectViewController* pvc = [ProjectViewController new];
    pvc.infoDictionary = infoDictionary;
    [self.navigationController pushViewController:pvc animated:YES];
    
}

-(void)tab:(id) sender
{
    
    static int count = 2;
    count ++;
    if (count % 2 !=0) {
        
        //Psuedo tabbar button
        tabBtn1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [tabBtn1 addTarget:self action:@selector(settings:)
          forControlEvents:UIControlEventTouchUpInside];
        [tabBtn1 setTitle:@"Settings" forState:UIControlStateNormal];
        [tabBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [[tabBtn1 layer] setBorderWidth:1.0f];
        [[tabBtn1 layer] setBorderColor:[UIColor blackColor].CGColor];
        tabBtn1.frame = CGRectMake(10, 610, 110, 50);
        tabBtn1.layer.cornerRadius = 10;
        tabBtn1.backgroundColor = [UIColor clearColor];
        [self.view addSubview:tabBtn1];
        
        //Psuedo tabbar button
        tabBtn3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [tabBtn3 addTarget:self action:@selector(socialMedia:)
          forControlEvents:UIControlEventTouchUpInside];
        [tabBtn3 setTitle:@"Share" forState:UIControlStateNormal];
        [tabBtn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [[tabBtn3 layer] setBorderWidth:1.0f];
        [[tabBtn3 layer] setBorderColor:[UIColor blackColor].CGColor];
        tabBtn3.frame = CGRectMake(250, 610, 110, 50);
        tabBtn3.layer.cornerRadius = 10;
        tabBtn3.backgroundColor = [UIColor clearColor];
        [self.view addSubview:tabBtn3];
    
    }
    
    else {
       
        tabBtn1.frame = CGRectMake(0, 0, 0, 0);
        tabBtn3.frame = CGRectMake(0, 0, 0, 0);
        
    }
        
}

-(void)settings:(id) sender
{
    SettingsViewController* svc = [SettingsViewController new];
        [self.navigationController pushViewController:svc animated:YES];
}

-(void)socialMedia:(id) sender
{
    SocialViewController* smvc = [SocialViewController new];
        [self.navigationController pushViewController:smvc animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return indicatorArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
             
        PFObject* tempObject = [indicatorArray objectAtIndex:indexPath.row];

    UILabel *textLbl = [[UILabel alloc]initWithFrame:CGRectMake(35, 10, self.view.frame.size.width - 30, 30)];
    textLbl.text = [tempObject objectForKey:@"IndicatorDescription"];
    textLbl.font = [textLbl.font fontWithSize:15];
    textLbl.backgroundColor = [UIColor clearColor];
    textLbl.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:textLbl];
    
//        cell.textLabel.text = [tempObject objectForKey:@"IndicatorDescription"];
//        cell.textLabel.font = [UIFont systemFontOfSize:12.0];
//        cell.backgroundColor = [UIColor clearColor];
//        cell.textLabel.textColor = [UIColor whiteColor];
//        cell.indentationLevel = cell.indentationLevel + 2;
    
    NSString* testPriority = [tempObject objectForKey:@"Priority"];
    if ([testPriority isEqual:@"High"]) {
        
        UIImageView* red = [[UIImageView alloc] initWithFrame:CGRectMake(8, 9, 30, 30)];
        red.backgroundColor=[UIColor clearColor];
        [red.layer setCornerRadius:8.0f];
        [red.layer setMasksToBounds:YES];
        [red setImage:[UIImage imageNamed:@"red"]];
        [cell.contentView addSubview:red];
        
        //NSLog(@"High");
    } else if ([testPriority isEqual:@"Medium"]) {
        
        UIImageView* yellow = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
        yellow.backgroundColor=[UIColor clearColor];
        [yellow.layer setCornerRadius:8.0f];
        [yellow.layer setMasksToBounds:YES];
        [yellow setImage:[UIImage imageNamed:@"yellow"]];
        [cell.contentView addSubview:yellow];
        
    } else {
        
        //NSLog(@"Low");
        
        UIImageView* green = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
        green.backgroundColor=[UIColor clearColor];
        [green.layer setCornerRadius:8.0f];
        [green.layer setMasksToBounds:YES];
        [green setImage:[UIImage imageNamed:@"green"]];
        [cell.contentView addSubview:green];

        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    NSDictionary* infoDictionary = arrayOfRecipies[indexPath.row];
//    
//    
//    infoViewController* info = [infoViewController new];
//    info.infoDictionary = infoDictionary;
//    [self.navigationController pushViewController:info animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
