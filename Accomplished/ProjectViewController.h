//
//  ProjectViewController.h
//  Accomplished
//
//  Created by Alex Brown on 7/21/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ProjectViewController : ViewController<UITableViewDataSource, UITableViewDelegate>
{
    
    NSArray* indicatorArray2;
    
    UILabel* titleLbl;
    
    UIButton* completeBtn;
    
    UIButton* addIndicatorBtn;
    
    UIView* addIndicatorView;
    
    UIButton* highPriorityBtn;
    UIButton* mediumPriorityBtn;
    UIButton* lowPriorityBtn;
    UIButton* saveIndicatorBtn;
    
    UITextView* indicatorTxt;
    
    NSString* priority2;
    UITableView* indicatorTableView;
 
}

@property (nonatomic, retain)
NSDictionary* infoDictionary;




@end
