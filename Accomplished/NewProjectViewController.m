//
//  NewProjectViewController.m
//  Accomplished
//
//  Created by Alex Brown on 7/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "NewProjectViewController.h"

@interface NewProjectViewController ()

@end

@implementation NewProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //Background view
    UIView* background = [[UIView alloc] initWithFrame:self.view.bounds];
    background.backgroundColor = [[UIColor alloc] initWithRed:0 green:.6 blue:.2 alpha:2];
    [self.view addSubview:background];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
