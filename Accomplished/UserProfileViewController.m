//
//  UserProfileViewController.m
//  Accomplished
//
//  Created by Alex Brown on 7/17/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "UserProfileViewController.h"

@interface UserProfileViewController ()

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Display navigation bar
    self.navigationController.navigationBarHidden = YES;
    
    //UIScrollView
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -50, self.view.bounds.size.width, 800)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height + 200);
    [self.view addSubview:scrollView];
    
    //Dismiss Keyboard
    UIGestureRecognizer* tapper = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    //Colors
    UIColor *color = [UIColor blackColor];

    //Background Views
    UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 200)];
    backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:.3];
    [scrollView addSubview:backgroundView];
    
    //Icon Image
    backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(150, 150, self.view.frame.size.width - 300, 70)];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFit;
    backgroundImage.clipsToBounds = YES;
    [backgroundImage setImage: [UIImage imageNamed:@"leaf"]];
    backgroundImage.alpha = 1.5;
    [scrollView addSubview:backgroundImage];
    
//    UIView* backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(170, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    backgroundView2.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:backgroundView2];
    
    //Profile label
//    UILabel* profileLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 75, self.view.frame.size.width - 20, 40)];
//    profileLbl.textAlignment = NSTextAlignmentLeft;
//    profileLbl.text = @"User Profile";
//    profileLbl.textColor = [UIColor lightGrayColor];
//    [profileLbl setFont:[UIFont fontWithName:@"Arial-BoldItalicMT" size:40]];
//    [self.view addSubview:profileLbl];
    
    //Name textfield
//    UITextField* nameTxt = [[UITextField alloc] initWithFrame:CGRectMake(30, 275, self.view.frame.size.width - 60, 40)];
//    nameTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-Full Name-" attributes:@{NSForegroundColorAttributeName: color}];
//    nameTxt.textAlignment = NSTextAlignmentCenter;
//    [nameTxt setBorderStyle:UITextBorderStyleBezel];
//    nameTxt.backgroundColor = [UIColor whiteColor];
//    nameTxt.layer.cornerRadius = 15;
//    [scrollView addSubview:nameTxt];
    
    //Username textfield
    usernameTxt = [[UITextField alloc] initWithFrame:CGRectMake(30, 325, self.view.frame.size.width - 60, 40)];
    usernameTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-User Name-" attributes:@{NSForegroundColorAttributeName: color}];
    usernameTxt.textAlignment = NSTextAlignmentCenter;
    [usernameTxt setBorderStyle:UITextBorderStyleBezel];
    usernameTxt.backgroundColor = [UIColor whiteColor];
    usernameTxt.layer.cornerRadius = 15;
    [scrollView addSubview:usernameTxt];
    
    //Password textfield
    passwordTxt = [[UITextField alloc] initWithFrame:CGRectMake(30, 375, self.view.frame.size.width - 60, 40)];
    passwordTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-Password-" attributes:@{NSForegroundColorAttributeName: color}];
    passwordTxt.textAlignment = NSTextAlignmentCenter;
    [passwordTxt setBorderStyle:UITextBorderStyleBezel];
    passwordTxt.backgroundColor = [UIColor whiteColor];
    passwordTxt.layer.cornerRadius = 15;
    [scrollView addSubview:passwordTxt];
    
    //Email textfield
//    emailTxt = [[UITextField alloc] initWithFrame:CGRectMake(30, 425, self.view.frame.size.width - 60, 40)];
//    emailTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"-Email-" attributes:@{NSForegroundColorAttributeName: color}];
//    emailTxt.textAlignment = NSTextAlignmentCenter;
//    [emailTxt setBorderStyle:UITextBorderStyleBezel];
//    emailTxt.backgroundColor = [UIColor whiteColor];
//    emailTxt.layer.cornerRadius = 15;
//    [scrollView addSubview:emailTxt];
    
    //Add image button
    UIButton* imageBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [imageBtn addTarget:self action:@selector(addImage:)
               forControlEvents:UIControlEventTouchUpInside];
    [imageBtn setTitle:@"Add User Image" forState:UIControlStateNormal];
    [imageBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [[imageBtn layer] setBorderWidth:2.0f];
    //[[imageBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    imageBtn.frame = CGRectMake(100, 475, self.view.frame.size.width - 200, 40);
    imageBtn.layer.cornerRadius = 15;
    imageBtn.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:imageBtn];
    
    //Submit new user
    UIButton* createProfileBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createProfileBtn addTarget:self action:@selector(createProfile:)
               forControlEvents:UIControlEventTouchUpInside];
    [createProfileBtn setTitle:@"Create User" forState:UIControlStateNormal];
    [createProfileBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [[createProfileBtn layer] setBorderWidth:2.0f];
    //[[createProfileBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    createProfileBtn.frame = CGRectMake(100, 505, self.view.frame.size.width - 200, 40);
    createProfileBtn.layer.cornerRadius = 15;
    createProfileBtn.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:createProfileBtn];
    
}

//Dismiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

//Add image action
-(void)addImage:(id) sender
{
    
}

//Create new user action
-(void)createProfile:(id) sender
{
        PFUser *user = [PFUser user];
        user.username = usernameTxt.text;
        user.password = passwordTxt.text;
        user.email = emailTxt.text;
        
        // other fields can be set just like with PFObject
//        user[@"phone"] = @"415-392-0202";
    
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {   // Hooray! Let them use the app now.
                ViewController *vc = [[ViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            } else {   NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
                UIAlertView* errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message: errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [errorAlert show];
                
            }
        }];
    
}

//Move view based on keyboard placement
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
