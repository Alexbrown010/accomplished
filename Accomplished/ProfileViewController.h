//
//  ProfileViewController.h
//  Accomplished
//
//  Created by Alex Brown on 7/17/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ProfileViewController : ViewController<UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    UIImageView* userImage;
    
    NSArray* arrayOfProjects;
    UIView* view;
    UIView* swipeView;
    UIView* newProjectView;
    UICollectionView* collectionView1;
    NSString* title;
    UITextView* descriptionTxt;
    UITextView* indicatorTxt;
    UIButton* saveIndicatorBtn;
    UITableView* tableview;
    UIButton* tabBtn1;
    UIButton* tabBtn3;
    UITextField* projectTitleTxt;
    NSArray* indicatorArray;
    NSArray* projectsArray;
    NSString* userId;
    UIButton* highPriorityBtn;
    UIButton* mediumPriorityBtn;
    UIButton* lowPriorityBtn;
    NSString* priority;
    UILabel* priorityLbl;
    
}

@end
