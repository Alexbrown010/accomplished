//
//  SocialViewController.m
//  Accomplished
//
//  Created by Alex Brown on 8/12/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "SocialViewController.h"

@interface SocialViewController ()

@end

@implementation SocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //Background views
    UIView* backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    backgroundView1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backgroundView1];
    
    UIView* backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 72, self.view.frame.size.width, self.view.frame.size.height)];
    backgroundView2.backgroundColor = [UIColor colorWithRed:120/255.0f green:173/255.0f blue:0/255.0f alpha:5.0f];
    [self.view addSubview:backgroundView2];

    //Black bar
    UIView* blackBar = [[UIView alloc] initWithFrame:CGRectMake(0, 70, self.view.frame.size.width, 2)];
    blackBar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:blackBar];
    
    //Back Button
    UIButton* backBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backBtn addTarget:self action:@selector(back:)
      forControlEvents:UIControlEventTouchUpInside];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [[backBtn layer] setBorderWidth:2.0f];
    //    [[backBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    backBtn.frame = CGRectMake(10, 20, 50, 40);
    backBtn.layer.cornerRadius = 10;
    backBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backBtn];
    
    //Project label
    UILabel* socialLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 35, self.view.frame.size.width, 40)];
    socialLbl.text = @"Social Media";
    socialLbl.textAlignment = NSTextAlignmentCenter;
    socialLbl.font = [socialLbl.font fontWithSize:30];
    [backgroundView2 addSubview:socialLbl];
}

-(void)back:(id) sender
{
   
    ProfileViewController* pvc = [ProfileViewController new];
    [self.navigationController pushViewController:pvc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
