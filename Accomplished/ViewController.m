//
//  ViewController.m
//  Accomplished
//
//  Created by Alex Brown on 7/16/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Dismiss Keyboard
    UIGestureRecognizer* tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    //Hide navigation bar
    self.navigationController.navigationBarHidden = YES;
    
    //Property List
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    pListArray = [[NSDictionary alloc]initWithContentsOfFile:filePath];
   
    //Colors
    UIColor* color = [UIColor whiteColor];
    
    //Background image
    backgroundImage = [[UIImageView alloc] initWithFrame: self.view.bounds];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImage.clipsToBounds = YES;
    [backgroundImage setImage: [UIImage imageNamed:[pListArray objectForKey:@"image"]]];
    backgroundImage.alpha = 1.5;
    [self.view addSubview:backgroundImage];
    
    //Title label
    UILabel* titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 320, self.view.frame.size.width - 10, 50)];
    titleLbl.text = @"Accomplished";
    [titleLbl setFont:[UIFont fontWithName:@"Arial-BoldItalicMT" size:40]];
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:titleLbl];
    
    //White bar
    UIView* whiteBar = [[UIView alloc] initWithFrame:CGRectMake(30, 360, self.view.frame.size.width - 30, 2)];
    whiteBar.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteBar];
    
    //Slide in login view
    loginView = [[UIView alloc] initWithFrame:CGRectMake(400, 0, 400, 700)];
    loginView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.7];
    [backgroundImage addSubview:loginView];
    
        //Back Button
    UIButton* backBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backBtn addTarget:self action:@selector(back:)
       forControlEvents:UIControlEventTouchUpInside];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[backBtn layer] setBorderWidth:2.0f];
    [[backBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    backBtn.frame = CGRectMake(10, 20, 50, 40);
    backBtn.layer.cornerRadius = 10;
    backBtn.backgroundColor = [UIColor clearColor];
    [loginView addSubview:backBtn];
    
        //Username textfield
    usernameTxt1 = [[UITextField alloc] initWithFrame:CGRectMake(10, 150, self.view.frame.size.width, 40)];
    usernameTxt1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    usernameTxt1.textAlignment = NSTextAlignmentLeft;
    //[usernameTxt1 setBorderStyle:UITextBorderStyleBezel];
    usernameTxt1.textColor = [UIColor whiteColor];
    //usernameTxt1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GreenLine"]];
    [loginView addSubview:usernameTxt1];
            //Green border
    greenLine = [[UIView alloc] initWithFrame:CGRectMake(10, 195, self.view.frame.size.width, 1)];
    greenLine.backgroundColor = [UIColor greenColor];
    greenLine.alpha = 3;
    [loginView addSubview:greenLine];
    
        //Password textfield
    userPasswordTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 200, self.view.frame.size.width, 40)];
    userPasswordTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    userPasswordTxt.textAlignment = NSTextAlignmentLeft;
    //[usernameTxt1 setBorderStyle:UITextBorderStyleBezel];
    userPasswordTxt.textColor = [UIColor whiteColor];
    userPasswordTxt.backgroundColor = [UIColor clearColor];
    [loginView addSubview:userPasswordTxt];
            //Green border
    greenLine2 = [[UIView alloc] initWithFrame:CGRectMake(10, 245, self.view.frame.size.width, 1)];
    greenLine2.backgroundColor = [UIColor greenColor];
    greenLine2.alpha = 3;
    [loginView addSubview:greenLine2];
    
    //Login to account
    UIButton* login2Btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [login2Btn addTarget:self action:@selector(login2:)
       forControlEvents:UIControlEventTouchUpInside];
    [login2Btn setTitle:@"Sign in" forState:UIControlStateNormal];
    [login2Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [[loginBtn layer] setBorderWidth:2.0f];
    //    [[loginBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    login2Btn.frame = CGRectMake(100, 270, self.view.frame.size.width - 200, 20);
    login2Btn.layer.cornerRadius = 10;
    login2Btn.backgroundColor = [UIColor clearColor];
    [loginView addSubview:login2Btn];
    
    //Slide in sign up view
    signUpView = [[UIView alloc] initWithFrame:CGRectMake(400, 0, 400, 700)];
    signUpView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.7];
    [backgroundImage addSubview:signUpView];
    
        //Back Button 2
    UIButton* back2Btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [back2Btn addTarget:self action:@selector(back2:)
                forControlEvents:UIControlEventTouchUpInside];
    [back2Btn setTitle:@"Back" forState:UIControlStateNormal];
    [back2Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[back2Btn layer] setBorderWidth:2.0f];
    [[back2Btn layer] setBorderColor:[UIColor grayColor].CGColor];
    back2Btn.frame = CGRectMake(10, 20, 50, 50);
    back2Btn.layer.cornerRadius = 10;
    back2Btn.backgroundColor = [UIColor clearColor];
    [signUpView addSubview:back2Btn];
    
        //name textfield
    nameTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 150, self.view.frame.size.width, 40)];
    nameTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full Name" attributes:@{NSForegroundColorAttributeName: color}];
    nameTxt.textAlignment = NSTextAlignmentLeft;
    //[nameTxt setBorderStyle:UITextBorderStyleBezel];
    nameTxt.textColor = [UIColor whiteColor];
    nameTxt.backgroundColor = [UIColor clearColor];
    [signUpView addSubview:nameTxt];
            //Green border
    greenLine = [[UIView alloc] initWithFrame:CGRectMake(10, 195, self.view.frame.size.width, 1)];
    greenLine.backgroundColor = [UIColor greenColor];
    greenLine.alpha = 3;
    [signUpView addSubview:greenLine];
    
        //Username textfield
    usernameTxt2 = [[UITextField alloc] initWithFrame:CGRectMake(10, 200, self.view.frame.size.width, 40)];
    usernameTxt2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    usernameTxt2.textAlignment = NSTextAlignmentLeft;
    //[usernameTxt2 setBorderStyle:UITextBorderStyleBezel];
    usernameTxt2.textColor = [UIColor whiteColor];
    //usernameTxt.backgroundColor = [UIColor whiteColor];
    [signUpView addSubview:usernameTxt2];
            //Green border
    greenLine2 = [[UIView alloc] initWithFrame:CGRectMake(10, 245, self.view.frame.size.width, 1)];
    greenLine2.backgroundColor = [UIColor greenColor];
    greenLine2.alpha = 3;
    [signUpView addSubview:greenLine2];
    
        //Password textfield
    userPasswordTxt2 = [[UITextField alloc] initWithFrame:CGRectMake(10, 250, self.view.frame.size.width, 40)];
    userPasswordTxt2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    userPasswordTxt2.textAlignment = NSTextAlignmentLeft;
    //[userPasswordTxt2 setBorderStyle:UITextBorderStyleBezel];
    userPasswordTxt2.textColor = [UIColor whiteColor];
    //userPasswordTxt.backgroundColor = [UIColor whiteColor];
    [signUpView addSubview:userPasswordTxt2];
            //Green border
    greenLine3 = [[UIView alloc] initWithFrame:CGRectMake(10, 295, self.view.frame.size.width, 1)];
    greenLine3.backgroundColor = [UIColor greenColor];
    greenLine3.alpha = 3;
    [signUpView addSubview:greenLine3];
    
        //email textfield
    emailTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 300, self.view.frame.size.width, 40)];
    emailTxt.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    emailTxt.textAlignment = NSTextAlignmentLeft;
    //[emailTxt setBorderStyle:UITextBorderStyleBezel];
    emailTxt.textColor = [UIColor whiteColor];
    //userPasswordTxt.backgroundColor = [UIColor whiteColor];
    [signUpView addSubview:emailTxt];
            //Green border
    greenLine4 = [[UIView alloc] initWithFrame:CGRectMake(10, 345, self.view.frame.size.width, 1)];
    greenLine4.backgroundColor = [UIColor greenColor];
    greenLine4.alpha = 3;
    [signUpView addSubview:greenLine4];
    
        //Submit user
    UIButton* createProfile2Btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createProfile2Btn addTarget:self action:@selector(createProfile2:)
        forControlEvents:UIControlEventTouchUpInside];
    [createProfile2Btn setTitle:@"Submit" forState:UIControlStateNormal];
    [createProfile2Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [[loginBtn layer] setBorderWidth:2.0f];
    //    [[loginBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    createProfile2Btn.frame = CGRectMake(100, 370, self.view.frame.size.width - 200, 20);
    createProfile2Btn.layer.cornerRadius = 10;
    createProfile2Btn.backgroundColor = [UIColor clearColor];
    [signUpView addSubview:createProfile2Btn];
    
    //Create a new user button
    UIButton* createProfileBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [createProfileBtn addTarget:self action:@selector(createProfile:)
               forControlEvents:UIControlEventTouchUpInside];
    [createProfileBtn setTitle:@"Sign up" forState:UIControlStateNormal];
    [createProfileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [[createProfileBtn layer] setBorderWidth:2.0f];
//    [[createProfileBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    createProfileBtn.frame = CGRectMake(100, 400, self.view.frame.size.width - 200, 20);
    createProfileBtn.layer.cornerRadius = 10;
    createProfileBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:createProfileBtn];
    
    //Login button
    UIButton* loginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [loginBtn addTarget:self action:@selector(login:)
       forControlEvents:UIControlEventTouchUpInside];
    [loginBtn setTitle:@"Already registered?" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [[loginBtn layer] setBorderWidth:2.0f];
//    [[loginBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    loginBtn.frame = CGRectMake(100, 430, self.view.frame.size.width - 200, 20);
    loginBtn.layer.cornerRadius = 10;
    loginBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:loginBtn];
    
    //Backdoor
//    UIButton* backdoor = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [backdoor addTarget:self action:@selector(backdoor:)
//       forControlEvents:UIControlEventTouchUpInside];
//    [backdoor setTitle:@"three" forState:UIControlStateNormal];
//    [backdoor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [[backdoor layer] setBorderWidth:2.0f];
//    [[backdoor layer] setBorderColor:[UIColor whiteColor].CGColor];
//    backdoor.frame = CGRectMake(10, 600, 110, 40);
//    backdoor.layer.cornerRadius = 10;
//    backdoor.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:backdoor];

}

//Backdoor
-(void)backdoor:(id) sender
{
    
}

//Back Button Action
-(void)back:(id) sender
{
    [self.view addSubview:loginView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [loginView setFrame:CGRectMake(400, 0, 400, 700)]; } completion:nil];
}

//Back2 Button Action
-(void)back2:(id) sender
{
    [self.view addSubview:signUpView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [signUpView setFrame:CGRectMake(400, 0, 400, 700)]; } completion:nil];
}

//Create Profile action
-(void)createProfile:(id) sender
{
    [self.view addSubview:signUpView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [signUpView setFrame:CGRectMake(0, 0, 400, 700)]; } completion:nil];
    
}

//Login button
-(void)login:(id) sender
{
    
    [self.view addSubview:loginView];
    
    [UIView animateWithDuration:1.0 delay:0.1f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{ [loginView setFrame:CGRectMake(0, 0, 400, 700)]; } completion:nil];

}

-(void)login2:(id) sender
{
    //    Check username and password with parse accounts
    [PFUser logInWithUsernameInBackground:usernameTxt1.text password:userPasswordTxt.text
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            // Do stuff after successful login
                                            ProfileViewController *pvc = [[ProfileViewController alloc] init];
                                            [self.navigationController pushViewController:pvc animated:YES];
                                        } else {
                                            // The login failed. Check error to see why.
                                            UIAlertView* errorAlert = [[UIAlertView alloc] initWithTitle:@"Login Error" message:@"There was a login error"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            [errorAlert show];
                                        }
                                    }];
    
}

-(void)createProfile2:(id) sender
{
    PFUser *user = [PFUser user];
    user.username = usernameTxt2.text;
    user.password = userPasswordTxt2.text;
    user.email = emailTxt.text;
    
    // other fields can be set just like with PFObject
    //        user[@"phone"] = @"415-392-0202";
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {   // Hooray! Let them use the app now.
            ViewController *vc = [[ViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        } else {   NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
            UIAlertView* errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message: errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
            
        }
    }];
}

//Dismiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

////Move view based on keyboard placement
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    [self animateTextField: textField up: YES];
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [self animateTextField: textField up: NO];
//}
//
//- (void) animateTextField: (UITextField*) textField up: (BOOL) up
//{
//    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
//    const float movementDuration = 0.3f; // tweak as needed
//    
//    int movement = (up ? -movementDistance : movementDistance);
//    
//    [UIView beginAnimations: @"anim" context: nil];
//    [UIView setAnimationBeginsFromCurrentState: YES];
//    [UIView setAnimationDuration: movementDuration];
//    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
//    [UIView commitAnimations];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
